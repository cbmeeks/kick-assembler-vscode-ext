# Release Notes
## v0.7.12
This version corrects the Kick version check by using a source file in the user temporary space instead of the default location of the vscode installation directory. In some instances this folder location would be locked (read-only) and no output would be generated as a result.

For a full list of changes, please see the projects [Changelog](CHANGELOG.md) file.

We hope you are enjoying the Extension, and if you find any bugs, or would like to see a certain feature added, please feel free to use our [Trello Board](https://trello.com/b/vIsioueo/kick-assembler-vscode-ext#) or the [Gitlab](https://gitlab.com/retro-coder/commodore/kick-assembler-vscode-ext/-/issues) issues page.

Enjoy!
